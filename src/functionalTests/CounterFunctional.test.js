import React from "react";
import { mount } from "enzyme";
import { store } from "../store";
import { Provider } from "react-redux";
import { findElementByDataTest } from "../testUtils";

import App from "../App";

const setup = () =>
  mount(
    <Provider store={store}>
      <App />
    </Provider>
  );

describe("does all components renders without fail", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup();
  });
  it("renders App component without fail", () => {
    const appComp = findElementByDataTest(wrapper, "app-component");
    expect(appComp.exists()).toBeTruthy();
  });
  it("renders counter component without fail", () => {
    const counterComp = findElementByDataTest(wrapper, "counter-component");
    expect(counterComp.exists()).toBeTruthy();
  });
  it("renders Button component without fail", () => {
    const buttonComp = findElementByDataTest(wrapper, "Button-component");
    expect(buttonComp.exists()).toBeTruthy();
  });
});

describe("perform increment and decrement actions and verift the states", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup();
  });
  it("intial state must be `0`", () => {
    const counterVal = findElementByDataTest(wrapper, `counter-value`);
    expect(counterVal.text()).toBe("0");
  });
  it("simulate an increment acton and verify the value to be `1`", () => {
    const button = findElementByDataTest(wrapper, `counter-button-increment`);
    button.simulate("click");
    const counterVal = findElementByDataTest(wrapper, `counter-value`);
    expect(counterVal.text()).toBe("1");
  });
  it("simulate an decrement acton and verify the value to be `1`", () => {
    const button = findElementByDataTest(wrapper, `counter-button-decrement`);
    button.simulate("click");
    const counterVal = findElementByDataTest(wrapper, `counter-value`);
    expect(counterVal.text()).toBe("0");
  });
});
