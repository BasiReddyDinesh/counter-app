import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { updateCounter } from "../actions";
import PropTypes from "prop-types";

import { INCREMENT, DECREMENT } from "../constants";
import Button from "../utils/Button";

function Counter(props) {
  const counter = useSelector((store) => store.counter);
  const dispatch = useDispatch();
  const increment = () => {
    dispatch(updateCounter(INCREMENT));
  };
  const decrement = () => {
    dispatch(updateCounter(DECREMENT));
  };
  return (
    <div data-test="counter-component">
      <h2 data-test="counter-value">{counter}</h2>
      <Button value={INCREMENT} clickHandler={increment} />
      <Button value={DECREMENT} clickHandler={decrement} />
    </div>
  );
}

Counter.propTypes = {};

export default Counter;
