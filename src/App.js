import "./App.css";
import Counter from "./components/Counter";
import { Provider } from "react-redux";

import { store } from "./store";

function App() {
  return (
    <div data-test="app-component" className="App">
      <Provider store={store}>
        <Counter />
      </Provider>
    </div>
  );
}

export default App;
