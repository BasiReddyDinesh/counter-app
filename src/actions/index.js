import { INCREMENT, DECREMENT } from "../constants";

const updateCounter = (actionType) => {
  if (actionType === INCREMENT) return { type: INCREMENT };
  return { type: DECREMENT };
};

export { updateCounter };
