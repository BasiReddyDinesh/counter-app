import { updateCounter } from ".";
import { INCREMENT, DECREMENT } from "../constants";

describe("testing payloads when differnent actions performed", () => {
  it("should return `INCREMENT` on INCREMENT action performed", () => {
    const payload = updateCounter(INCREMENT);
    expect(payload).toStrictEqual({ type: INCREMENT });
  });
  it("should return `DECREMENT` on DECREMENT action performed", () => {
    const payload = updateCounter(DECREMENT);
    expect(payload).toStrictEqual({ type: DECREMENT });
  });
});
