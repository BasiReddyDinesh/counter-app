import React from "react";
import { shallow } from "enzyme";

import { findElementByDataTest, checkProps } from "../testUtils";

import Button from "./Button";

const clickHandler = jest.fn();

const defaultProps = {
  value: "increment",
  clickHandler,
};

const setup = (props = { ...defaultProps }) => {
  return shallow(
    <Button value={props.value} clickHandler={props.clickHandler} />
  );
};

describe("render Button component without fail", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup({
      value: "increment",
      clickHandler,
    });
  });
  it("render Button without fail", () => {
    const buttonComp = findElementByDataTest(wrapper, "Button-component");
    expect(buttonComp).toHaveLength(1);
  });
  it("render button without fail", () => {
    const button = findElementByDataTest(wrapper, "counter-button-increment");
    expect(button.exists()).toBeTruthy();
  });
  it("simulate click on button", () => {
    const button = findElementByDataTest(wrapper, "counter-button-increment");
    button.simulate("click");
    expect(clickHandler).toHaveBeenCalled();
  });
});

it("does not throw warring with expected props", () => {
  checkProps(Button, defaultProps);
});
