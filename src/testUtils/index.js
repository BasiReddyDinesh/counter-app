import checkPropTypes from "check-prop-types";

const findElementByDataTest = (wrapper, val) => {
  return wrapper.find(`[data-test='${val}']`);
};

const checkProps = (component, compProps) => {
  const propErr = checkPropTypes(
    component.PropTypes,
    compProps,
    "prop",
    component.name
  );
  expect(propErr).toBe(undefined);
};

export { findElementByDataTest, checkProps };
