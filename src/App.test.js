import React from "react";
import { shallow } from "enzyme";
import { findElementByDataTest } from "./testUtils";

import App from "./App";

const setup = () => shallow(<App />);

test("renders App component without fail", () => {
  const wrapper = setup();
  const appComp = findElementByDataTest(wrapper, "app-component");
  expect(appComp.exists()).toBeTruthy();
});
