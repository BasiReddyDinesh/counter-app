import { INCREMENT, DECREMENT } from "../constants";

const counterReducer = (counter = 0, action) => {
  switch (action.type) {
    case INCREMENT:
      return counter + 1;
    case DECREMENT:
      return counter - 1;
    default:
      return 0;
  }
};

export { counterReducer };
