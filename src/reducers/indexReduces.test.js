import { INCREMENT, DECREMENT } from "../constants";
import { counterReducer } from ".";

describe("testing reducers state when different acctions performed", () => {
  it("should return defauult counterState when no ation triggred", () => {
    const state = counterReducer(0, { type: "" });
    expect(state).toBe(0);
  });
  it("should return 1 counterState when INCREMENT ation triggred", () => {
    const state = counterReducer(0, { type: INCREMENT });
    expect(state).toBe(1);
  });
  it("should return -1 counterState when DECREMENT ation triggred", () => {
    const state = counterReducer(0, { type: DECREMENT });
    expect(state).toBe(-1);
  });
});
