import { store } from "../store";
import { INCREMENT, DECREMENT } from "../constants";

describe("testing store counter on dispatching different acctions", () => {
  it("intial store counter must be 0", () => {
    const expectedStore = { counter: 0 };
    expect(expectedStore).toStrictEqual(store.getState());
  });
  it("dispatch an INCREMENT and verify the counter in store to be 1", () => {
    store.dispatch({ type: INCREMENT });
    const expectedStore = { counter: 1 };
    expect(expectedStore).toStrictEqual(store.getState());
  });
  it("dispatch an INCREMENT and verify the counter in store to be 0", () => {
    store.dispatch({ type: DECREMENT });
    const expectedStore = { counter: 0 };
    expect(expectedStore).toStrictEqual(store.getState());
  });
});
